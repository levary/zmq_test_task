import zmq
import logging
from sys import argv
from time import sleep
from string import ascii_uppercase
from random import choice, seed

def randString():
    seed()
    message = ''.join(choice(ascii_uppercase) for x in xrange(20))
    return message

class Client:
    def __init__(self, addr = '127.0.0.1', port = 4444, msg_number = 5):
        try:
            self.context = zmq.Context()
            self.socket = self.context.socket(zmq.REQ)
            self.socket.connect('tcp://%s:%s' % (str(addr), str(port)))
            self.msg_number = int(msg_number)

            logging.basicConfig(format = '%(message)s', level = logging.INFO)
        except Exception:
            print 'error: unsuccessful initialisation'
            raise

    def send(self):
        status = 0

        try:
            for x in xrange(self.msg_number):
                self.socket.send(randString())
                response = self.socket.recv()
                if response == 'OK':
                    status |= 0
                else:
                    status |= 1
                logging.info(response)
                sleep(1)
        except Exception:
            print 'error: unsuccessful socket operation'
            raise

        return status

    def __del__(self):
        self.socket.close()
        self.context.destroy()

def main():
    status = 0
    init_data = argv[1:]

    try:
        client = Client(*init_data)
        status = client.send()
    except Exception as e:
        print e.message
        quit(1)

    if status:
        print 'messages transfer FAILED'
    else:
        print 'SUCCESS'
    quit(status)

if __name__ == '__main__':
    main()
