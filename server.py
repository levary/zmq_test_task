import zmq
import logging
import pymongo
import signal
import time
from sys import argv
from pymongo import MongoClient

fl_to_listen = 1

class Server:
    def __init__(self, port = 4444, host_db = '127.0.0.1', port_db = 27017, name_db = 'messages_db'):
        try:
            # Init socket
            self.context = zmq.Context()
            self.socket = self.context.socket(zmq.REP)
            self.socket.bind('tcp://127.0.0.1:%s' %str(port))

            # Init DB
            self.client = MongoClient(host_db, int(port_db))
            self.db = self.client[name_db]
            self.msgs = self.db.messages

            # Init logger
            logging.basicConfig(format = '[%(asctime)s] %(message)s', level = logging.INFO)
        except pymongo.errors.PyMongoError:
            print 'error: unsuccessful database initialisation'
            raise
        except Exception:
            print 'error: unsuccessful initialisation'
            raise
        
    def run(self):
        global fl_to_listen

        while fl_to_listen:
            try:
                data = self.socket.recv()
                logging.info('%s' % data)
                # Save to DB
                record = {"time": time.asctime(), "message": data}
                self.msgs.save(record)
                
                self.socket.send('OK')
            except pymongo.errors.OperationFailure as e:
                print 'error: unsuccessful database operation'
                print e.message
                self.socket.send('FAIL')
            except Exception as e:
                print 'error: unsuccessful message processing'
                print e.message
                self.socket.send('FAIL')
    
    def __del__(self):
        self.socket.close()
        self.context.destroy()

def signal_handler(signum, frame):
    global fl_to_listen 
    fl_to_listen = 0

    signal.signal(signal.SIGTERM, signal.SIG_DFL)
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    print '\nprogram is finished'
    quit(0)

def main():
    init_data = argv[1:]

    signal.signal(signal.SIGTERM, signal_handler)
    signal.signal(signal.SIGINT, signal_handler)

    try:
        server = Server(*init_data)
        server.run()   
    except Exception as e:
        print e.message
        quit(1)
    quit(0)

if __name__ == '__main__':
    main()
